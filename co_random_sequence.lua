local function co_random_sequence(data, start, finish, step, stop_it)
  math.randomseed(os.time())
  local normal_sequence = {}
  for i = start, finish, step do
    table.insert(normal_sequence, i)
  end
  local function iterator()
    for i = start, finish, step do
      if stop_it.now then
        return true
      else
        local index = table.remove(normal_sequence, math.random(#normal_sequence))
        coroutine.yield(data[index])
      end
    end
  end
  return coroutine.create(iterator)
end

return co_random_sequence