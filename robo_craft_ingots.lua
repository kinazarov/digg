local ns = {}

local c = require("component")
local r = c.robot
local s = require("sides")
local args, opts = require("shell").parse(...)

local goOn = true
local m = 0
local nuggets_per_craft = 1

r.select(1)
if args[1] then
    nuggets_per_craft = tonumber(args[1])
end

while goOn do
    for i = 1, 12 do
        m = math.fmod(i, 4)
        if m > 0 then
            goOn = goOn and r.suck(s.front, nuggets_per_craft)
        end
        r.select(r.select() + 1)
    end
    r.select(1)
    if goOn then
        for i = 1, nuggets_per_craft do
            c.crafting.craft()
        end
        r.drop(s.down, 64)
    else
        for i = 1, 12 do
            m = math.fmod(i, 4)
            if m > 0 then
                r.drop(s.front, nuggets_per_craft)
            end
            r.select(r.select() + 1)
        end
    end
end
r.select(1)