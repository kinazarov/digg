local args = {...}
local start_offset = 2
local component = require("component")
if #args == 1 then
  local offset = tonumber(args[1])
  if offset then start_offset = offset end
end

for addr, _ in pairs(component.list("warpdriveMiningLaser", true)) do
  local ml = component.proxy(addr)
  ml.offset(start_offset)
  ml.silktouch(true)
  ml.start()
end