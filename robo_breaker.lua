local c = require("component")
local r = c.robot
local s = require("sides")
local me = c.isAvailable("upgrade_me")
if me then me = c.upgrade_me end
local inv_map = {}


for i = 1, r.inventorySize() do
    local items = r.count(i)
    if items ~= 0 then
        inv_map[i] = math.floor(items)
    end
end

for slot , blockCount in pairs(inv_map) do
    r.select(slot)
    for j = 1, blockCount do
        local placed = r.place(s.down)
        if placed then
            local broken, block_type = r.swing(s.down)
            if broken then
                r.suck(s.down)
            end
        else
            print('Cannot place block in slot ', slot)
            return -1
        end
    end
end

os.sleep(2)

if me then
    for i = 1, r.inventorySize() do
        if r.count(i) ~= 0 then
            r.select(i)
            me.sendItems()
        end
    end
end

r.select(1)