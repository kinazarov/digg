states = {}


--#####
function start(args)
  states.cmp = require("component")
  states.evt = require("event")
  local s = require("sides")
  local ic = states.cmp.inventory_controller
  local tb = states.cmp.tractor_beam
  local r = states.cmp.robot

  local chest_side = s.down
  local container_side = s.front
  local check_timeout = 5 --sec
  local stash_size = 20000

  r.select(1)

  function states.container_is_full()
    local is_full = true
    local size = ic.getInventorySize(container_side)
    if size then
      for i = 1, size do
        local stack = ic.getStackInSlot(chest_side, i)
        if stack == nil or stack.size < stash_size then
          is_full = false
        end
      end
    else
      is_full = false
    end
    return is_full
  end

  function states.check_and_dismantle()
    if states.container_is_full() then
      r.use(s.front)
      tb.suck()
      r.drop(chest_side)
    end
  end

  states.timer_handle = states.evt.timer(check_timeout, check_and_dismantle, math.huge)
end


--#####
function stop(args)
  states.evt.cancel(states.timer_handle)
end