timer = 0

function start(args)
  local cmp = require("component")
  local s = require("sides")
  local r = cmp.robot
  local liquid = cmp.tank_controller
  local event = require("event")
  local rlevel = 1200
  local rtimeout = 5

  r.use(s.down)
  local function check_fluid()
    local level = liquid.getTankLevel(s.front)
    if level >= rlevel then
      r.drain(s.front, 1000)
      r.fill(s.down, 1000)
    end
    r.use(s.down)
  end
  timer = event.timer(rtimeout, check_fluid, math.huge)
end

function stop()
  require("event").cancel(timer)
end