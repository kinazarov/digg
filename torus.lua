local R = 8
local r = 4

local function torus_func(x, y, z, R, r, threshhold)
	local t = (x^2 + y^2 + z^2 + R^2 - r^2)^2 - 4 * (R^2) * (x^2 + y^2)
	if t < threshhold then return "*" else return " " end
end

for z = -1 -r, 1 + r do
	for y = -R - r, R + r do
		for x = -R - r, R + r do
			io.write(string.format("%2s", torus_func(x, y, z, R, r, 0)))
		end
		io.write("\n")
	end
	io.write("****\n")
end