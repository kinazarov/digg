timer = 0

function start()
   if not args then args = {} end
   local stop_level = args.stop_level or 0.95
   local start_level = args.start_level or 0.2
   print(args.start_level)
   local interval = args.interval or 4
   local s = require("sides")
   local cmp = require("component")
   local mfe = args.mfe or cmp.mfe
   local controller = args.redstone or cmp.redstone
   local reactor_side = args.reactor_side or s.right
   local event = require("event")

   local capacity = mfe.getEUCapacity()

   local function setActivity()
      local energy = mfe.getEUStored()
      local level = energy / capacity
      if level > stop_level then
         controller.setOutput(reactor_side, 0)
      end
      if level < start_level then
         controller.setOutput(reactor_side, 15)
      end
   end

   timer = event.timer(interval, setActivity, math.huge)
end

function stop()
   local cmp = require("component")
   local event = require("event")
   local s = require("sides")
   local controller = args.redstone or cmp.redstone
   local reactor_side = args.reactor_side or s.right
   event.cancel(timer)
   timer=0
   controller.setOutput(reactor_side, 0)
end