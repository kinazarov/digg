local ndex = {[1]="cobble", [2]="mud"}
--{[index] = "item_name_as_inv_controller_reads_it"}
--two first inventory slots has contained cobble and mud when robot detect it

robot_inv_table =
{
  [1] = {{1,5},{2,2},{3,2},{4,2},{5,8}}, -- [item_id] = {{inventory_slot,quantity},{inventory_slot,quantity},...{inventory_slot,quantity}}
  [2] = {{6,3},{7,2},{8,3}}
}

local test_materials = {"cobblestone", "mud"}

local test_layer ={
  {{1,5}},
  {1,{2,3},1},
  {1,2,0,2,1},
  {1,{2,3},1},
  {{1,5}}
  }
--[[
defines this layer
1 1 1 1 1
1 2 2 2 1
1 2 e 2 1
1 2 2 2 1
1 1 1 1 1

Where 1 and 2 is different blocks and e is empty space
]]

--reqs
local cmp --= require("component")
local s --= require("sides")
--debug overrides
local dt = require("dt")
cmp = dt.set(cmp)
s = dt.sides


-- const
local slot_num = 1
local quantity = 2

local function reverse(tbl)
  local result = {}
  local index, value = next(tbl)
  while index do
    table.insert(result,1,value)
    index, value = next(tbl, index)
  end
  return result
end

local function pick_inv(mat_record, robot)
  local i, v = next(mat_record)
  if i then
    robot.select(v[slot_num])
  end
end

local function decrease_inv(mat_record)
  local slot_record = mat_record[1]
  if slot_record then
    if slot_record[quantity] == 1 then
      table.remove(mat_record, 1)
    else
      slot_record[quantity] = slot_record[quantity] - 1
    end
  else
    error("no_materials_left")
  end
end

local function build_layer(robot, sides, materials, layer, turn_right, reverse_needed)
  turn_right = turn_right or true
  reverse_needed = reverse_needed or true
  local reversing = false
  local block, repeat_placing
  for line_number, line in ipairs(layer) do
    if reverse_needed and reversing then line = reverse(line) end
    for block_num, building_block in ipairs(line) do
      repeat_placing = 1
      if type(building_block) == "table" then
        block = building_block[slot_num]
        repeat_placing = building_block[quantity]
      else
        block = building_block
      end
      local mat_record = materials[block]
      for i = 1, repeat_placing do
        if block>0 then
          pick_inv(mat_record, robot)
          if robot.place(sides.down) then
            robot.move(sides.forward)
            decrease_inv(mat_record)
          else
            --error handling
            --should wait beeping until obstacle removed by player
            error("cannot place")
          end
        else
          robot.move(sides.forward)
        end
      end
    end
    robot.turn(turn_right)
    robot.move(sides.forward)
    robot.turn(turn_right)
    turn_right = not turn_right
    reversing = not reversing
  end
end

local function scan_for_materials(robot, inv_controller, blocktypes)
  --[[
  --blocktypes example
  {
  ["minecraft.cobblestone"] = function(r, ic, slot) r.drop(require("sides").down) end, --drop while scanning inventory
  ["minecraft.stone"] = true, --use
  ["minecraft.redstoneblock"] = false --ignore
  }
  ]]
  local compared, mat_record
  local result, indexer, empties, other = {}, {}, {}, {}
  for i = 1, robot.inventorySize() do
    robot.select(i)
    local slot_info = inv_controller.getStackInInternalSlot(i)
    if blocktypes then
      compared = blocktypes[slot_info.name]
    elseif slot_info then
      compared = slot_info.name
    else
      compared = nil
    end
    if type(compared) == "function" then
      compared(robot, inv_controller, slot_info)
    else
      if compared then
        local index = indexer[slot_info.name]
        if not index then
          table.insert(index, slot_info.name)
          index = #result
        end
        mat_record = result[index] or {}
        table.insert(mat_record, {index, slot_info.size})
        result[index] = mat_record
      elseif compared == false then
        table.insert(others,i)
      else
        table.insert(empties, i)
      end
    end
  end
  return result, indexer
end


local r = cmp.robot
local ic = cmp.inventory_controller

build_layer(r,s,robot_inv_table,test_layer)