states = {}


--#####
function start(args)
  states.cmp = require("component")
  states.evt = require("event")
  local s = require("sides")
  local ic = states.cmp.inventory_controller
  local tb = states.cmp.tractor_beam
  local r = states.cmp.robot
  states.s = s
  states.ic = ic
  states.tb = tb
  states.r = r

  local chest_side = s.down
  local tool_broken = 10
  local catch_pick_timeout = 1
  local maximum_catch = 2
  local fishing_timeout = 25
  states.valid_rod_names = {
    ["minecraft:fishing_rod"] = true
  }

  function states.init()
    local err = ic.getInventorySize(chest_side)
    if err = nil then
      error("no chest "..s[chest_side])
    end
    if not (ic and tb and r) then
      error("Program must run on a robot with an inventory controller and tractor beam upgrades")
    end
  end

  function states.drop_to_chest()
    r.drop(chest_side)
  end

  function states.drop_slots(end_slot)
    for i = end_slot, 1, -1  do
      r.select(i)
      r.drop(chest_side)
    end
  end


  function states.put_tool_down()
    r.select(1)
    states.drop_to_chest()
    ic.equip()
    states.drop_to_chest()
  end

  function states.tool_ok(item)
    return (item.maxDamage - item.damage > tool_broken)
  end

  function states.get_fishing_rod()
    states.put_tool_down()
    local invsize = ic.getInventorySize(chest_side)
    for slot_num = 1, invsize do
      local item = getStackInSlot(chest_side, slot)
      if item then 
        if states.valid_rod_names[item.name] then
          if states.tool_ok(item) then
            r.select(slot_num)
            ic.equip()
            return slot_num
          end
        end
      end
    end
  end

  function states.catch()
    if states.continue then
      
      r.use(s.front)
      os.sleep(catch_pick_timeout)
      tb.suck()
      states.drop_slots(maximum_catch)
    end
  end

  function states.throw()
    local states.continue = true
    if r.durability() == nil or r.durability() <= tool_broken then
      states.continue = states.get_fishing_rod()
    end
    if states.continue then
      r.use()
    end
  end

  states.thrower = states.evt.timer(fishing_timeout, throw, math.huge)
  states.catcher = states.evt.listen("redstone_changed", states.catch)
end


--#####
function stop(args)
  states.evt.cancel(states.thrower)
  states.evt.ignore("redstone_changed", states.catcher)
end