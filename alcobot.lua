local alco = {}
-- ===
--local com = require("component")

-- ===debug section
local dt = require("dt")
local com = dt.set()
-- ===debug section

local robot = com.robot
local ic = com.inventory_controller

local barrel_state = {}

local function comparator(number, type)
  local func
  if type == 'eq' then
    func = function(input) return input == number end
    return func
  elseif type == 'gt' then
    func = function(input) return input > number end
    return func
  elseif type == 'gte' then
    func = function(input) return input >= number end
    return func
  else
  end
end

function alco.get_user_input()
  local max_wheat, max_hops, max_water = 64, 64, 32
  local beer_types = {
    brewtype = {'beer', 'rum'},
    thickness = {
      watery = comparator(0,'gt'),
      lite = comparator(0.5,'gt'),
      [''] = comparator(1,'eq'),
      strong = comparator(1,'gt'),
      thick = comparator(2,'gte'),
      stodge = comparator(2.4,'gte'),
      ['black stuff'] = comparator(4,'gte'),
      order = {'watery', 'lite', '', 'strong', 'thick', 'stodge', 'black stuff'}
    },
    hops_to_wheat = {
      soup = comparator(0,'gte'),
      ['alcogol free'] = comparator(1/3,'gte'),
      white = comparator(0.5,'gte'),
      [''] = comparator(1,'gte'),
      dark = comparator(2,'gte'),
      full = comparator(3,'gte'),
      black = comparator(4,'gte'),
      order = {'soup', 'alcohol free', 'white', '', 'dark', 'full', 'black'}
    },
    brewtime = {
      brew = comparator(0,'gte'),
      youngster = comparator(0.5,'gte'),
      beer = comparator(2,'gte'),
      ale = comparator(12,'gte'),
      dragonblood = comparator(24,'gte'),
      order = {'brew', 'youngster', 'beer', 'ale', 'dragonblood'}
    }
  }

  local function calculate_on_water(thickness_cmp, hops_to_wheat_cmp, maximum_water)
    local acceptable = {}
    local thickness, h2w, stop
    maximum_water = maximum_water or max_water
    for water = 1, maximum_water do
      print(water)
      for hops = 1, max_hops do
        for wheat = 1, max_wheat do
          thickness = (hops + wheat) / water
          h2w = hops / wheat
          if not stop and thickness_cmp(thickness) and hops_to_wheat_cmp(h2w) then
            acceptable.hops = hops
            acceptable.water = water
            acceptable.wheat = wheat
            stop = true
          end
        end
      end
      stop = false
    end
    return acceptable
  end
end

local function prepare_brewing(type, ratios)

end

-- ===debug section
--alco.get_user_input()
-- ===debug section

return alco