local function rotateSubMatrix(basicArray, subWidth, direction, skipRows, skipCols, basicWidth, basicHeight, clearWith)
  if not subWidth then return end

  direction = direction or -1
  skipRows = skipRows or 0
  skipCols = skipCols or 0
  basicWidth = basicWidth or subWidth
  basicHeight = basicHeight or basicWidth

  local function getCorners(w, rp, skipRows, skipCols, basicWidth)
    if not w then
      return
    end
    if math.abs(rp) >= w then
      return
    end
    skipRows = skipRows or 0
    skipCols = skipCols or 0
    rp = rp or 0
    basicWidth = basicWidth or w
    rp = rp or 0
    local corner1 = basicWidth * skipRows + skipCols
    local corner2 = corner1 + w - 1
    local corner3 = corner1 + basicWidth * (w -1)
    local corner4 = corner3 + w - 1
    if rp ~= 0 then
       corner1 = corner1 + rp
       corner2 = corner2 + rp * basicWidth
       corner3 = corner3 - rp * basicWidth
       corner4 = corner4 - rp
     end
    return {corner1, corner2, corner4, corner3}
  end

  local rotorFunc
  local tmp
  if direction<0 then
    rotorFunc = function(array, points)
      tmp = array[points[1]]
      array[points[1]] = array[points[2]]
      array[points[2]] = array[points[3]]
      array[points[3]] = array[points[4]]
      array[points[4]] = tmp
    end
  else
    rotorFunc = function(array, points)
      tmp = array[points[4]]
      array[points[4]] = array[points[3]]
      array[points[3]] = array[points[2]]
      array[points[2]] = array[points[1]]
      array[points[1]] = tmp
    end
  end
  for i = 0, subWidth/2 do
    local sqSide = subWidth - i*2
      for j = 0, sqSide - 2 do
        local corners = getCorners(sqSide,j,skipRows + i,skipCols + i,basicWidth)
        rotorFunc(basicArray, corners)
      end
  end
  if clearWith then
    for i = 0, basicHeight - 1 do
      for j = 0, basicWidth - 1 do
        local inSubSquare = (i >= skipRows and i < skipRows + subWidth) and (j >= skipCols and j < skipCols + subWidth)
        if not inSubSquare then
          basicArray[i*basicWidth + j] = clearWith
        end
      end
    end
  end
end

return {rotateSubMatrix}