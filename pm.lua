local pm = {}
local robot, side, ic, objInfo, tb, pc
local matType = 1
local materialCounter = 0
local swingsCounter = 0
local materialStash
local emptySlots
local toolsSwingable
local toolsUsable
local swingCheckThreshhold = 5
local swingsCounter = 0
local storageModes = {letItRot = 1, pickAndSignalWhenFull = 2, pickAndSBringToStorage = 3}
local maxTries = 4
local retryTime = 0.5
local unloadTime = 5
local digggingMoveMode = false
local digggingBuilderMode = true
local pickStoreMode = storageModes.letItRot

local signals = {
	  noMaterial = {440, 0.6, 0.4},
	  unload = {1318.51, 0.2, 0.8},
	  default = {493.88, 0.4}
}
local function beepPattern(params)
   return {
   	{params[1], params[2]},
   	{[2] = params[3] or params[2]/2},
   	{params[4] or params[1], params[5] or params[2]}
   	}
end

local function beep(params)
   params = params or beepPattern(signals.default)
   for _, v in ipairs(params) do
      if v[1] then
         pc.beep(v[1], v[2])
      else
         os.sleep(v[2])
      end
   end
end

local function obstacleAction(condition,actname,oside)
   if condition then
      if not robot.detect(oside) then -- if not passable mside
         return {actname, pm.dig, oside}
      end
   end
end

function move(mside)
   local moves = {
      [side.forward] = 'mf',
      [side.back] = 'mb',
      [side.up] = 'mu',
      [side.down] = 'md'
   }
   local moveName = moves[mside]
   local act = {moveName, robot.move, mside}
   local preMove = obstacleAction(digggingMoveMode, '_d' .. moveName, mside)
   if preMove then
      return try({preMove, act})
   else
      return try(act)
   end

end

function pm.forward()
   return move(side.forward)
end
function pm.back()
   return move(side.back)
end

function pm.up()
   return move(side.up)
end

function pm.down()
   return move(side.down)
end

function pm.turnLeft()
   robot.turn(false)
   return {'tl', true}
end

function pm.turnRight()
   robot.turn(true)
   return {'tr', true}
end

function pm.turnAround()
   robot.turn(false)
   robot.turn(false)
   return {'ta', true}
end

local function checkTool()
   local res = true
   if swingsCounter < swingCheckThreshhold then
      swingsCounter = swingsCounter - 1
   else
      local dur = robot.durability()
      if dur then
         swingsCounter = dur
      else
         if #toolsSwingable > 0 then
            local currentSlot = robot.select()
            local toolData = table.remove(toolsSwingable)
            robot.select(toolData[1])
            swingsCounter = toolData[2].maxDamage - toolData[2].damage
            robot.equip()
            table.insert(emptySlots, toolData[1])
            robot.select(currentSlot)
         else
            res = false
         end
      end
   end
   return res
end

local function unloadAction()
   local qty
   repeat
      qty = robot.count()
      print('unload required')
      beep(beepPattern(signals.unload))
      os.sleep(unloadTime)
   until qty > 0
end

local function canStore()
   local slot
   for i = 1, #emptySlots do
      slot = emptySlots[#emptySlots]
      if robot.count(slot) ~= 0 then
         table.remove(emptySlots)
      else
         return true
      end
   end
   return false
end

local function storeAction(aside)
   if storeMode ~= storageModes.letItRot then
      if canStore() then
         if tb then
            tb.suck()
         else
            robot.suck(aside)
         end
      else
         unloadAction()
      end
   end
end

local function dig(dside)
   local digSites = {
      [side.forward] = 'df',
      [side.up] = 'du',
      [side.down] = 'dd'
   }
   local digActionName = digSites[dside]
   if checkTool() then
      try(digActionName, robot.swing, dside)
      storeAction(dside)
   else
      return {digActionName, false, 'no tools left'}
   end
end

function pm.dig()
   return dig(side.forward)
end

pm.digForward = pm.dig

function pm.digUp()
   return dig(side.up)
end

function pm.digDown()
   return dig(side.down)
end

local function checkMaterials(materialType)
   local function carelessDecrease()
      materialCounter = materialCounter - 1
   end

   materialType = materialType or 1

   if materialType == matType and materialCounter > 0 then
      return carelessDecrease
   else
      matType = materialType
      local mData = materialStash[matType]
      if mData then
         mData = table.remove(mData)
         robot.select(mData[1])
         materialCounter = mData[2]
      else
         local slotData
         -- here is a good place to save task execution state to a file in control
         -- computer filesystem in case we going to run out of energy and possibly resume
         -- execution long after
         repeat
            beep(beepPattern(signal.noMaterials))
            os.sleep(2)
            if ic then
               print('Need more ' .. matType)
               slotData = ic.getStackInInternalSlot()
               if slotData.name == matType then
                  materialCounter = slotData.size
               end
            else
               slotData = robot.count()
               if slotData > 0 then
                  materialCounter = slotData
               end
            end
         until materialCounter > 0
      end
   end
end

function place(pside, material)
   local places = {
      [side.forward] = 'pf',
      [side.up] = 'pu',
      [side.down] = 'pd'
   }
   local breakBlockAction
   local placeActionName = places[pside]
   if pm.chooseMaterial(material) then
      if digggingBuilderMode then
         breakBlockAction = obstacleAction(digggingBuilderMode, '_d' .. placeActionName, pside)
         return try(breakBlockAction, {placeActionName, robot.place, pside})
      else
         return {placeActionName, true,'block already present'}
      end
   else
      return {placeActionName, false,'no building blocks left'}
   end
end

function pm.place(material)
   return place(side.forward, material)
end

function pm.placeUp(material)
   return place(side.up, material)
end

function pm.placeDown(material)
   return place(side.down, material)
end

function pm.signalNoBuildMaterials()
end

function pm.sortIventory()
   local slotData = {name = 1}
   materialStash = {}
   toolsSwingable = {}
   materialCounter = 0
   swingsCounter = 0
   toolsUsable = {}
   emptySlots = {}
   for InvSlotIndex = 1, robot.inventorySize() do
      robot.select(InvSlotIndex)
      if ic then
         --if there is inventory_controller we sort items depending on its data
         slotData = ic.getStackInInternalSlot(InvSlotIndex)
         if slotData then
            if objInfo.isSwingableTool(slotData.name) then
               toolsSwingable[#toolsSwingable + 1] = {InvSlotIndex, slotData}
            elseif objInfo.isUsableTool(slotData.name) then
               toolsUsable[#toolsUsable + 1] = {InvSlotIndex, slotData}
            end
         end
      else
         -- otherwise count all items as materials
         slotData.size = robot.count()
      end
      if slotData and slotData.size > 0 then
         local stash = materialStash[slotData.name]
         stash[#stash + 1] = {InvSlotIndex, slotData}
         materialStash[slotData.name] = stash
      else
         table.insert(emptySlots, InvSlotIndex)
      end
   end
   local toolStatus = checkTool()
   return {swingableToolPresent = toolStatus}
end

local function try(actions)
   local res, err, actname, act, arg, cnt = maxTries
   if type(actions[1]) == 'table' then
      res = {}
      for i, v in ipairs(actions) do
         res[i] = try(v)
      end
   else
      actname = actions[1]
      act = actions[2]
      arg = actions[3]
      while cnt > 0 do
         res, err = act(arg)
         if res == true then
            cnt=0
         else
            os.sleep(retryTime)
         end
      end
      cnt = cnt - 1
      res = {actname, res, err}
   end
   return res
end

function pm.init(components,sidesTable,digMode)
   robot = components.robot
   side = sidesTable
   if components.isAvaliable("inventory_controller") then
      ic = components.inventory_controller
   end
   if components.isAvaliable("tractor_beam") then
      tb = components.tractor_beam
   end
   pc = components.computer
   objInfo = require("objectsInfo")
   return pm.sortIventory()
end

pm.maxTries = maxTries
pm.retryTime = retryTime
pm.digggingMoveMode = digggingMoveMode
pm.digggingBuilderMode = digggingBuilderMode
pm.pickStoreMode = pickStoreMode
pm.swingCheckThreshhold = 5

return pm